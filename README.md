# MongoDB "Snake BnB" application
### AirBnb for snake owners - this application allows snake owners with extra space in their snake cages to rent out that space.
Basically, a simple mockup of the AirBnb platform using MongoDB and mongoEngine ODM.
Content created by following along with http://freemongodbcourse.com/