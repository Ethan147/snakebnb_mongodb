from colorama import Fore
from infrastructure.switchlang import switch
import infrastructure.state as state
import services.data_service as svc
from dateutil import parser as parser


def run():
    print(' ****************** Welcome host **************** ')
    print()

    show_commands()

    while True:
        action = get_action()

        with switch(action) as s:
            s.case('c', create_account)
            s.case('a', log_into_account)
            s.case('l', list_cages)
            s.case('r', register_cage)
            s.case('u', update_availability)
            s.case('v', view_bookings)
            s.case('m', lambda: 'change_mode')
            s.case(['x', 'bye', 'exit', 'exit()'], exit_app)
            s.case('?', show_commands)
            s.case('', lambda: None)
            s.default(unknown_command)

        if action:
            print()

        if s.result == 'change_mode':
            return


def show_commands():
    print('What action would you like to take:')
    print('[C]reate an account')
    print('Login to your [a]ccount')
    print('[L]ist your cages')
    print('[R]egister a cage')
    print('[U]pdate cage availability')
    print('[V]iew your bookings')
    print('Change [M]ode (guest or host)')
    print('e[X]it app')
    print('[?] Help (this info)')
    print()


def create_account():
    print(' ****************** REGISTER **************** ')
    name = input('What is your name? ').strip().lower()
    email = input('What is your email? ').strip().lower()

    old_account = svc.find_account_by_email(email)
    if old_account:
        error_msg(f"ERROR: Acount with email {email} already exists.")
        return

    state.active_account = svc.create_account(name,email)
    success_msg(f"Created new account with id {state.active_account.id}.")

def log_into_account():
    print(' ****************** LOGIN **************** ')
    email = input("What is your email?").strip().lower()
    account = svc.find_account_by_email(email)

    if account is None:
        error_msg(f"Account does with email {email} does not exist.")
        return

    state.active_account = account
    success_msg(f"You've logging in as {email}.")


def register_cage():
    print(' ****************** REGISTER CAGE **************** ')

    if not state.active_account:
        error_msg("An account is required to register a cage.")
        return

    meters = input('How many square meters is the cage?')
    if not meters:
        error_msg("Cage registration cancelled.")
        return

    meters = float(meters)
    isCarpeted = input("Is the cage carpeted [y,n]?").strip().lower().startswith('y')
    hasToys = input("Does it have snake toys [y,n]").strip().lower().startswith('y')
    allow_dangerous = input("Can you host venomous snakes [y,n]?").strip().lower().startswith('y')
    name = input("Give your cage a name:").strip().lower()
    price = float(input("How much are you charging?").strip().lower())

    cage = svc.register_cage(
            active_account = state.active_account,
            name = name,
            meters = meters,
            isCarpeted = isCarpeted,
            hasToys = hasToys,
            allow_dangerous = allow_dangerous,
            price = price)
    state.reload_account()
    success_msg(f"Registered new cage with id {cage.id}.")


def list_cages(supress_header=False):
    if not supress_header:
        print(' ******************     Your cages     **************** ')

    if not state.active_account:
        error_msg("You must login to register an account.")
        return

    cages = svc.find_cages_for_user(state.active_account)
    print(f"You have {len(cages)} cages.")

    for idx, c in enumerate(cages):
        print(f" {idx+1}. {c.name} is {c.square_meters} meters.")
        print("lenn bookings " + str(len(c.bookings)))
        for b in c.bookings:
            booked = True if (b.booked_date is not None) else False
            print(f"    * Booking: {b.check_in_date}, {b.check_out_date-b.check_in_date} days, booked? {booked}")


def update_availability():
    print(' ****************** Add available date **************** ')

    if state.active_account is None:
        error_msg("Login to update your availability.")
        return

    list_cages(supress_header=True)    

    cages = svc.find_cages_for_user(state.active_account)

    cage_number = int(input("Enter cage number: ").strip())
    if not cage_number or cage_number>len(cages) or cage_number <= 0:
        error_msg("Cancelled")
        print()
        return

    selected_cage = cages[cage_number-1]    
    success_msg(f"Selected cage {selected_cage.name}")
    start_date = parser.parse(input("Enter available date [yyyy-mm-dd]: "))
    days = int(input("How many days is this block of time? "))

    svc.add_available_date(
        selected_cage,
        start_date,
        days
    )

    svc.add_available_date(selected_cage,start_date,days)
    state.reload_account()
    success_msg(f"Date added to cage {selected_cage.name}.")



def view_bookings():
    print(' ****************** Your bookings **************** ')

    # TODO: Require an account
    # TODO: Get cages, and nested bookings as flat list
    # TODO: Print details for each

    print(" -------- NOT IMPLEMENTED -------- ")


def exit_app():
    print()
    print('bye')
    raise KeyboardInterrupt()


def get_action():
    text = '> '
    if state.active_account:
        text = f'{state.active_account.name}> '

    action = input(Fore.YELLOW + text + Fore.WHITE)
    return action.strip().lower()


def unknown_command():
    print("Sorry we didn't understand that command.")


def success_msg(text):
    print(Fore.LIGHTGREEN_EX + text + Fore.WHITE)


def error_msg(text):
    print(Fore.LIGHTRED_EX + text + Fore.WHITE)
